//
//  ViewController.swift
//  Tinder
//
//  Created by Уали on 1/16/19.
//  Copyright © 2019 Ualikhan. All rights reserved.
//

import UIKit

class HomeController: UIViewController {

    let topStackView = TopNavigationStackVIew()
    let cardsDeckView = UIView()
    let buttonsStackView = HomeBottomControlsStackView()
    
//    let users = [
//        User(name: "Kelly", age: 23, profession: "Music DJ", imageName: "lady5c"),
//        User(name: "Jane", age: 18, profession: "Teacher", imageName: "lady4c")
//    ]
    
//    let cardViewModels = ([
//        User(name: "Kelly", age: 23, profession: "Music DJ", imageName: "lady5c"),
//        User(name: "Jane", age: 18, profession: "Teacher", imageName: "lady4c"),
//        Advertiser(title: "Slide out menu", brandName: "Lets Build That App", posterPhotoName: "slide_out_menu_poster")
//        ] as [ProducesCardViewModel]).map { (producer) -> CardViewModel in
//            return producer.toCardViewModel()
//    }
    
    let cardViewModels: [CardViewModel] = {
        let producers = [
            User(name: "Kelly", age: 23, profession: "Music DJ", imageNames: ["kelly1","kelly2","kelly3"]),
            Advertiser(title: "Slide out menu", brandName: "Lets Build That App", posterPhotoName: "slide_out_menu_poster"),
            User(name: "Jane", age: 18, profession: "Teacher", imageNames: ["jane1","jane2","jane3"])
        ] as [ProducesCardViewModel]
        
        let viewModels = producers.map({return $0.toCardViewModel()})
        return viewModels
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topStackView.settingsButton.addTarget(self, action: #selector(handleSetting), for: .touchUpInside)
        
        setupLayout()
        setupDummyCards()
        
    }
    
    @objc func handleSetting(){
        print("show registration page")
        let registrationController = RegistrationController()
        present(registrationController, animated: true, completion: nil)
    }
    
    fileprivate func setupDummyCards(){
        print("Setting up dummy cards")
        
        cardViewModels.forEach { (cardVM) in
            let cardView = CardView(frame: .zero)
            
            cardView.cardViewModel = cardVM
            
//            cardView.imageView.image = UIImage(named: cardVM.imageName)
//            cardView.informationLabel.attributedText = cardVM.attributedString
//            cardView.informationLabel.textAlignment = cardVM.textAlignment
            
            cardsDeckView.addSubview(cardView)
            cardView.fillSuperview()
        }
     
        
    }
    
    //MARK:- Fileprivate

    fileprivate func setupLayout() {
        let overallStackView = UIStackView(arrangedSubviews: [topStackView,cardsDeckView,buttonsStackView])
        //        stackView.distribution = .fillEqually
        overallStackView.axis = .vertical
        
        view.addSubview(overallStackView)
        overallStackView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
        overallStackView.isLayoutMarginsRelativeArrangement = true
        overallStackView.layoutMargins = .init(top: 0, left: 12, bottom: 0, right: 12)
        
        //stoit vperedi vsex
        overallStackView.bringSubviewToFront(cardsDeckView)
    }

}


